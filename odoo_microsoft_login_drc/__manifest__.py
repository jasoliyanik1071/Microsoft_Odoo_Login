# -*- coding: utf-8 -*-
{
    'name': 'Odoo Microsoft Login',
    'summary': '''Login using Microsoft Account''',
    'description': ''' Login using Microsoft Account ''',
    'author': "DRC Systems India Pvt. Ltd.",
    'website': "http://www.drcsystems.com",
    # 'category': 'Website',
    'version': '0.1',
    'depends': ['auth_oauth', 'website', 'base'],
    'data': [
        'data/microsoft_provider_data.xml',
        'views/microsoft_configuration_view.xml',
    ],
    'images': ['static/description/icon.png'],
    'installable': True,
    'auto_install': False,
    'application': True,
}
