# -*- coding: utf-8 -*-
import json
import werkzeug.urls
from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import serialize_exception
from odoo.addons.auth_oauth.controllers.main import OAuthLogin


class MicrosoftProviderHostChange(OAuthLogin):

    def list_providers(self):
        try:
            providers = request.env['auth.oauth.provider'].sudo().search_read([('enabled', '=', True)])
        except Exception:
            providers = []
        for provider in providers:
            if 'is_microsoft_account' in provider and provider.get('is_microsoft_account'):
                microsoft_provider = request.env['auth.oauth.provider'].sudo().search([('is_microsoft_account', '=', True), ('enabled', '=', True)], limit=1)
                return_url = microsoft_provider.redirect_uri
            else:
                return_url = request.httprequest.url_root + 'auth_oauth/signin'
            state = self.get_state(provider)
            params = dict(
                response_type="token",
                client_id=(provider['client_id']),
                redirect_uri=(return_url),
                scope=(provider['scope']),
                state=json.dumps(state),
            )
            provider['auth_link'] = "%s?%s" % (provider['auth_endpoint'], werkzeug.url_encode(params))
        return providers


class OAuthController(http.Controller):

    @http.route('/auth_oauth/signin', type='http', auth='none', methods=['GET', 'POST'])
    @serialize_exception
    def microsoft_signin(self, **kw):
        import pdb
        pdb.set_trace()
        print 'custom controller'
