# -*- coding: utf-8 -*-

from odoo.exceptions import UserError
from odoo import api, fields, models, _

OUTLOOK_AUTH_ENDPOINT = 'https://login.microsoftonline.com/common/oauth2/v2.0/authorize'


class MicrosoftAuthOauthProvider(models.Model):

    _inherit = "auth.oauth.provider"

    client_secret = fields.Char(string="Client Secret", default="koYbewa0PB4YcX9MG6dkyYd")
    redirect_uri = fields.Char(string="Your application redirect URI", default="http://localhost:8069/auth_oauth/signin")
    state = fields.Char(default="5fdfd60b-8457-4536-b20f-fcb658d19458", help="A long unique string value of your choice that is hard to guess. Used to prevent CSRF.")
    is_microsoft_account = fields.Boolean(string="Is a Microsoft Account", default=False)

    @api.model
    def create(self, value):
        if 'is_microsoft_account' in value and value.get('is_microsoft_account'):
            is_microsoft_account = self.env['auth.oauth.provider'].search([('is_microsoft_account', '=', True)])
            if is_microsoft_account:
                raise UserError(_('Only one Microsoft Account Enabled at a time...'))
            if not is_microsoft_account.enabled:
                raise UserError(_('You must enabled this aacount on clicking Enabled...'))
        elif 'enabled' in value and self.is_microsoft_account:
            if not value['enabled']:
                raise UserError(_('You must enabled this aacount on clicking Enabled...'))

        res = super(MicrosoftAuthOauthProvider, self).create(value)
        return res

    @api.multi
    def write(self, vals):
        if 'is_microsoft_account' in vals and vals.get('is_microsoft_account') and 'enabled' not in vals:
            is_microsoft_account = self.env['auth.oauth.provider'].search([('is_microsoft_account', '=', True)])
            if is_microsoft_account:
                raise UserError(_('Only one Microsoft Account Enabled at a time...'))
            if vals['is_microsoft_account'] and not self.enabled:
                raise UserError(_('You must enabled this aacount on clicking Enabled...'))
        elif 'enabled' in vals and self.is_microsoft_account and 'is_microsoft_account' not in vals:
            if not vals['enabled']:
                raise UserError(_('You must enabled this aacount on clicking Enabled...'))

        res = super(MicrosoftAuthOauthProvider, self).write(vals)
        return res


class BaseConfigSettings(models.TransientModel):

    _inherit = 'base.config.settings'

    auth_signup_uninvited = fields.Boolean(default=True)

    @api.model
    def default_get(self, fields):
        res = super(BaseConfigSettings, self).default_get(fields)
        res['auth_signup_uninvited'] = True
        if self.env['res.config.settings']:
            self.env['res.config.settings'][0].execute()
        return res
